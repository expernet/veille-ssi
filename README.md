# L'intelligence Artificielle et la Cyber-sécurité

L'intelligence artificielle prend de plus en plus de place tant dans le monde physique que dans le monde numérique.  
La Cyber-sécurité est aussi un sujet qui gagne en importance, et les utilisateurs de tous les jours s'en soucient d'autant plus que tout devient digitalisé.  
Mais l'Intelligence Artificelle a-t-elle sa place dans le monde de la Cyber-sécurité?

![https://towardsdatascience.com/no-machine-learning-is-not-just-glorified-statistics-26d3952234e3](https://miro.medium.com/max/500/1*x7P7gqjo8k2_bj2rTQWAfg.jpeg)

* **Temps de lecture estimé :** 18 minutes.
* **Prérequis :** Sensibilisé à la Cyber-sécurité
* **Définitions :**
  * **IA :** Intelligence Artificielle
  * **ML :** Machine Learning
  * **DL :** Deep Learning
  * **Dataset :** Jeu de données

## Contexte

### L'Intélligence Artificielle

#### Description 

Voyons brièvement ce qu'est l'intelligence artificielle.

![ia_ml_dl.png](./image_7.png)

* L'**Intelligence Artificielle** est une science qui étudie les moyens de construire des programmes et des machines intelligentes qui peuvent résoudre des problèmes de manière créative.
* Le **Machine Learning** est un sous-ensemble de l'Intelligence Artificielle (IA) qui fournit aux systèmes la capacité d'apprendre et de s'améliorer automatiquement à partir d'expérience sans être explicitement programmé. Dans le ML, il existe différents algorithmes (par exemple les réseaux de neuronaux) qui aident à résoudre des problèmes.
* Le **Deep Learning** est un sous-ensemble du Machine Learning, qui utilise les réseaux neuronaux pour analyser différents facteurs avec une structure similaire au système neuronal humain.

La forme la plus commune est le Machine Learning, cependant, le Deep Learning se répand de plus en plus.

##### Classification et Régression

| Classification | Régression |
|-----------------|-----------|
| ![https://medium.com/deep-math-machine-learning-ai/different-types-of-machine-learning-and-their-types-34760b9128a2](./image_5.png) | ![https://medium.com/deep-math-machine-learning-ai/different-types-of-machine-learning-and-their-types-34760b9128a2](./image_6.png) |

On peut dire que la classification sépare les données et les catégorises, la régression s'approche de la donnée voulue.

#### L'apprentissage

Voici ce dont nous avons principalement besoin pour faire du Machine Learning :
* Données
* Algorithmes

#### Les différentes applications
Voici les différentes utilisations de l'Intelligence Artificelle actuellement.

![image_8.png](./image_8.png)

Nous pouvons noter qu'il n'y a pas vraiment de rubriques spécialisée dans la cyber-sécurité.

### Les besoins en Cyber-sécurité

De manière générale, la sécurité d'une entreprise demande de :
* Identifier de potentiels problèmes
  * Scan de vulnérabilités
  * Pentest
  * Analyse de risques
* Détecter les menaces
  * IDS/IPS
  * Analyse de logs
  * Analyse d'événements
  * Analyse de comportement
  * Scan de hash, de paquets, etc.
* Protéger
  * Prise de décision
  * Action de prévention ou corrective

Toute l'infrastructure possède des besoins qui ont été énoncés précédemment. Cela fait beaucoup de données à analyser et une organisation doit **beaucoup investir** en argent et en temps pour garder toute l'entreprise sécurisé.  

Comme nous l'avons vu, l'intélligence artificelle se nourrit de données pour gagner en expérience et devenir plus précise et efficace.

Nous allons alors nous pencher sur les différentes utilisations possibles de l'intelligence artificielle *pour* la Cyber-sécurité et voir si l'IA a réellement sa place dans la Cyber-sécurité.

## Attaque

### Pentest
Nous savons que les pentests sont utiles pour évaluer la sécurité et les mesures de sécurité en place. Mais un pentest c'est aussi :
* Un coût financier qui est souvent dû au temps passé
* Des tâches parfois répétivies avec quelques changements

#### l'IA pour le pentest

![image_2.png](./image_2.png)

L'Intelligence Artificielle apporte :
* Gain de temps
* Meilleur retour sur investissement (ROI)
  * Réduction des besoins en externalisations
  * Les experts peuvent se concentrer sur l'analyse des root causes et sur les éléments qui empêchent la création de vulnérabilités
* Meilleure efficience
  * Test à la demande sans délai d'exécution
  * Fourni des résultats aux équipes de développement en quelques heures, et non en quelques semaines et colle avec du SDLC *(Secure Development LifeCycle)*
* Meilleure sécurité globale
  * Tester toutes les applications web
  * Tester avec un couvrage similaire à celui d'un humain

#### Exemple de solution

* nimis.ai
  * Deep Reinforcement Learning : permet de gérer les différents niveaux de complexité de la tâche
  * Identification de vulnérabilités : découverte rapide et sans impact fort en charge sur le système
  * Moteur de validation : valide la vulnérabilité potentielle en tant que problème réel qui doit être traité

### Social engineering, Phishing, Vol d'identité

Il est plus facile de créer de fausses identités, plus crédibles, en utilisant l'Intelligence Artificielle.  
On peut perfectionner les techniques actuelle comme en changeant le visage de quelqu'un sur une vidéo, par exemple avec DeepFake.

#### DeepFake

DeepFake est la combinaison de **Deep** Learning et de **Fake** qui permet de remplacer le visage d'une personne dans une vidéo ou image par une autre.

Ce genre de failcification vidéo/audio peut augmenter grandement les chances de succès des attaques de type phishing, social engineering et de vol d'identité. DeepFake a déjà fait la contreverse pour avoir été utilisé sur des vidéos pornographiques, pour des "revenge porn" mais aussi pour des **fraudes financières**.

**Exemple**

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/HmEtFTEQGwo" frameborder="0" allowfullscreen="true" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"> </iframe>
</figure>

**Fraude financière**  
En 2019, un PDG du Royaume-Uni, s'est fait arnaqué par la version audio de DeepFake par téléphone, où il lui a été demandé par la voix d'une personne de confiance un transfert de 220 000€.

**Détection**  
La technique la plus populaire de détection consiste à utiliser des algorithmes similaires à ceux utilisés pour construire DeepFake. En mettant en évidence les méthodes utilisées par DeepFake, des algorithmes sont créés et capables de détecter de subtiles incohérences. Les chercheurs ont mis au point des systèmes automatiques qui examinent les vidéos pour détecter les erreurs telles que les clignotements de lumière irréguliers.

### Captcha
Les Intelligences Artificielles peuvent aider à valider différentes versions de captchas, et ceci, automatisable dans des scripts pour faciliter des attaques de différents types.  
  
Voici des exemples de solutions d'IA pour les captchas:

| Type de captcha  | Screenshot | Exemple de solution IA | Langage / API | 
|------------------|-------------|-----------|------------------------|
| Captcha simple | ![fb_captcha_example.png](./image_11.png) | github.com/nladuo/captcha-break : Tesseract-OCR | C++ & Python |
| Google reCaptcha  | ![google_recaptcha_example.png](./image_12.png) | github.com/alexandster/Break-reCaptcha : TensorFlow | Python |

## Défense

### Classification de Malwares
**STAMINA Deep Learning for Malware Protection** est le produit de la collaboration de Microsoft et Intel. 
> [source](https://www.microsoft.com/security/blog/2020/05/08/microsoft-researchers-work-with-intel-labs-to-explore-new-deep-learning-approaches-for-malware-classification/)

Microsoft et Intel ont eu une idée intéressante qui est de convertir les malwares en images, puis d'utiliser l'experience existante dans l'analyse d'images des Intelligences Artificielles afin de traiter celles-ci.

![image_14.png](./image_14.png)

Les tests globaux effectués ont performé à 99.07% de précision et 2.58% de faux positifs.

### Banques
*[source principale](https://spd.group/machine-learning/fraud-detection-with-machine-learning/#Credit_Card_Fraud_Detection_with_Machine_Learning)*

Les banques utilisent déjà l'Intelligence Artificielle pour repérer des transactions frauduleuses. Grâce au grand nombre de données que les banques possèdent et aussi de part leurs ressources financières, elles peuvent mettre en place des IA efficaces qui peuvent nous protéger au mieux contre les actions frauduleuses.

![https://spd.group/machine-learning/fraud-detection-with-machine-learning/#Credit_Card_Fraud_Detection_with_Machine_Learning](./image_13.png)

Le rapport de l'IC3 (Internet Crime Complaint Center) pour 2019 montre que les victimes ont perdu près de 112 millions de dollars à cause de la fraude par carte de crédit.  
La fraude à la carte est la fraude de paiement la plus commune car les transactions sont difficilement vérifiables.  

Voici les différents types de fraude à la carte :
1. Vol de carte de crédit
2. FormJacking
3. Vol de compte
4. Interception d'emails
5. Demandes de crédits frauduleuses: vol d'identité pour faire ces démarches

#### Les solutions d'IA

Voici quelques solutions possibles qui reposent sur l'Intelligence Artificelle pour pallier à ces problèmes :
* Utilisation d'algorithmes de détection d'anomalies.
* Détection de phishing par email en se basant sur un jeu de données contenant des emails légitimes et des emails de phishing.
* Analyse de comportement des utilisateurs pour repérer les comportements suspects.

#### Cas réel

Nous pouvons prendre comme exemple, Almundo.com qui est une plateforme de voyage en ligne d'Amérique latine  qui a **réduit de 70% les refacturations et les revues manuelles grâce au Machine Learning**.

### Gestion de risques

Ici, nous allons prendre l'exemple de Balbix ([balbix.com](balbix.com)) qui permet :
* d'explorer et d'inventorier les différents actifs, 
* de les suivres à travers différents vecteurs d'attaques possibles ,
* d'analyser les risques et tenter de prédire les scénarii d'attaques les plus probables,
* et enfin de prioriser les actions correctives.

![https://www.balbix.com/](./image.png)

> 88% des brèches sont dues à une mauvaise cyber-hygiène. Balbix aide à faire évoluer la Cyber-sécurité d'une entreprise et à réduire les risques de brèches de 95% ou plus.

La surface d'attaque d'une entreprise est massive et il existe un grand nombre de moyens par lesquels les attaquants peuvent tenter de pénétrer le SI de celle-ci. Pour avoir une idée précise des risques de brèche, les équipes de Cyber-sécurité doivent analyser de nombreuses données - jusqu'à plusieurs centaines de milliards de signaux variables dans le temps provenant du réseau, de dispositifs en palce, d'applications et d'utilisateurs.

Analyser et améliorer le niveau de Cyber-sécurité n'est plus vraiment un problème à échelle humaine.

Grâce à une IA spécialisée, Balbix recueille et analyse automatiquement des informations complètes sur les actifs et leurs menaces. La plateforme permet de traiter un large éventail de cas d'utilisation de la gestion des vulnérabilités et des risques qui contribuent à faire évoluer le niveau de sécurité de l'entreprise.

Ce type de solution permet d'améliorer :
* Les contrôles et les mesures en place pour protéger l'entreprise des cyber-attaques
* La capacité à gérer les défenses
* La disponibilité et la capacité à réagir et à se remettre des événements impactant la sécurité.

![image_risk.png](./image_risk.png)

### Logs

L'analyse de logs est lourde, longue et fastidieuse. Nous devons souvent passer à travers des millers ou millions de lignes pour repérer d'éventuelles anomalies. Cependant, les Intelligence Artificielle sont faites pour les gros nombres de données.

#### LogPAI

Le but ultime de LogPAI est de construire une plateforme d'IA open-source pour l'analyse automatisée des logs. Pour atteindre cet objectif, LogPAI effectue une analyse comparative d'un ensemble de travaux de recherche que leur équipe publie avec des datasets et des outils open-source pour la recherche en matière d'analyse des logs. Avec les datasets et le code source disponibles, leur équipe espère que le projet LogPAI pourra profiter à la fois aux chercheurs et aux techniciens.

Leurs outils d'analyse des logs ont été utilisés par des équipes industrielles de Microsoft et de Huawei.

## Dangers
![sicara.ai/blog/artificial-general-intelligence](./image_15.png)

Il faut cependant garder à l'esprit que les IA n'ont pas toujours raison et il faut rester vigilent sur leur résultats.

Comme toutes technologies, l'Intelligence Artificielle comporte des failles. En effet, on peut faire du data poisoning ou encore exploiter des faiblesses dans leur précisions comme il a déjà été montré avec la reconnaissance faciale.

## Conclusion
De manière générale, la cyberdéfense s'appuie sur le fait que nous devons avoir quelques coups d'avance par rapport aux attanquants. Les technologies évoluent tellement vite, et les personnes malveillantes sont toujours plus inventives. Là où l'Intelligence Artificielle prend son intérêt, c'est qu'elle apprend plus rapidement que les humains.

L'IA prend à coup sûr sa place dans la Cyber-sécurité. Il faut cependant rester prudent quant à son utilisation et son développement. Nous savons qu'une IA n'est pas parfaite et elle devrait rester, en grande majorité, une aide à la décision ou un facilitateur et accélérateur de tâches plus ou moins répétitives/redondantes.

La place que va occuper l'Intelligence Artificelle dans le monde et surtout dans la Cyber-sécurité va grandir jusqu'à ce qu'elle devienne incontournable. Tant dans la défense que dans l'attaque.











